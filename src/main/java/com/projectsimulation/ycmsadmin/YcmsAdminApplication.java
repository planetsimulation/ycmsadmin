package com.projectsimulation.ycmsadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer
public class YcmsAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(YcmsAdminApplication.class, args);
	}

}
